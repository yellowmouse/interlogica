﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Pasticceria2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Pasticceria2.Controllers
{
    public class IngredientiController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<Ingredienti> Get()
        {
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            using (var context = new PASTICCERIAEntities())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;

                var query = context.Ingredienti.ToList();
                return query;
            }
        }

        // GET api/<controller>/5
        public Ingredienti Get(int id)
        {
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            using (var context = new PASTICCERIAEntities())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;

                var query = context.Ingredienti.Where((elem) => elem.ID_Ingrediente == id).FirstOrDefault();
                return query;
            }
        }

        // POST api/<controller>
        public HttpResponseMessage Post([FromBody]string value)
        {
            var obj = JObject.Parse(value);
            using (var context = new PASTICCERIAEntities())
            {
                if (Int32.Parse(obj.GetValue("ID_Ingrediente").ToString()) == 0)
                {
                    //Inserimento

                    var ingrediente = new Ingredienti();
                    ingrediente.Nome_Ingrediente = obj.GetValue("Nome_Ingrediente").ToString();
                    context.Ingredienti.Add(ingrediente);
                    context.SaveChanges();

                }
                else
                {
                    //Modifica
                    var id = Int32.Parse(obj.GetValue("ID_Ingrediente").ToString());
                    var ingrediente = context.Ingredienti.Where((elem) => elem.ID_Ingrediente == id).First();
                    ingrediente.Nome_Ingrediente = obj.GetValue("Nome_Ingrediente").ToString();
                    context.SaveChanges();
                }
            }
            return new HttpResponseMessage()
            {
                Content = new StringContent("Ingrediente creato")
            };
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)  
        {
            using (var context = new PASTICCERIAEntities())
            {
                var ingrediente = context.Ingredienti.Where((ing) => ing.ID_Ingrediente == id).First();
                if (ingrediente != null)
                {
                    context.Ingredienti.Remove(ingrediente);
                    context.SaveChanges();
                }
            }
        }
    }
}