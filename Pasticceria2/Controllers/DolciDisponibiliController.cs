﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Pasticceria2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Pasticceria2.Controllers
{
    public class DolciDisponibiliController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<JObject> Get()
        {
           GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            using (var context = new Models.PASTICCERIAEntities())
            {
                context.Configuration.LazyLoadingEnabled = true;
                context.Configuration.ProxyCreationEnabled = true;

                var query = (from dd in context.DolciDisponibili
                             join dolce in context.Dolci on dd.ID_Dolce equals dolce.ID_Dolce
                             orderby dd.Data_Vendita ascending
                             select new
                             {
                                 dd.ID,
                                 dd.ID_Dolce,
                                 dolce.Nome,
                                 dd.Prezzo_Vendita,
                                 dd.Qta,
                                 dd.Data_Vendita
                             }).ToArray();
                var retArr = new List<JObject>();
                for (var i = 0; i < query.Length; i++)
                {
                    retArr.Add(JObject.Parse(JsonConvert.SerializeObject(query[i])));
                }

                return retArr;

            }
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            using (var context = new Models.PASTICCERIAEntities())
            {
                context.Configuration.LazyLoadingEnabled = true;
                context.Configuration.ProxyCreationEnabled = true;

                var query = (from dd in context.DolciDisponibili
                             join dolce in context.Dolci on dd.ID_Dolce equals dolce.ID_Dolce
                             where dd.ID == id
                             select new
                             {
                                 dd.ID,
                                 dd.ID_Dolce,
                                 dolce.Nome,
                                 dd.Prezzo_Vendita,
                                 dd.Qta,
                                 dd.Data_Vendita
                             }).FirstOrDefault();

                return JsonConvert.SerializeObject(query);

            }
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
            var obj = JObject.Parse(value);
            using (var context = new PASTICCERIAEntities())
            {
                if (Int32.Parse(obj.GetValue("ID").ToString()) == 0)
                {
                    //Inserimento
                    var dolciDisponibili = new DolciDisponibili();
                    dolciDisponibili.ID_Dolce = Int32.Parse(obj.GetValue("ID_Dolce").ToString());
                    dolciDisponibili.Prezzo_Vendita  = Int32.Parse(obj.GetValue("Prezzo_Vendita").ToString());
                    dolciDisponibili.Qta = Int32.Parse(obj.GetValue("Dolce_Quantita").ToString());
                    dolciDisponibili.Data_Vendita = obj.GetValue("Data_Vendita").Value<DateTime>();
                    context.DolciDisponibili.Add(dolciDisponibili);
                    context.SaveChanges();
                }
                else
                {
                    //Modifica
                    var id = Int32.Parse(obj.GetValue("ID").ToString());
                    var dolciDisponibili = context.DolciDisponibili.Where((elem) => elem.ID == id).First();
                    dolciDisponibili.ID_Dolce = Int32.Parse(obj.GetValue("ID_Dolce").ToString());
                    dolciDisponibili.Prezzo_Vendita = Int32.Parse(obj.GetValue("Prezzo_Vendita").ToString());
                    dolciDisponibili.Qta = Int32.Parse(obj.GetValue("Dolce_Quantita").ToString());
                    dolciDisponibili.Data_Vendita = obj.GetValue("Data_Vendita").Value<DateTime>();
                    context.SaveChanges();
                }
            }
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            using (var context = new PASTICCERIAEntities())
            {
                var dolceDisponibile = context.DolciDisponibili.Where((ing) => ing.ID == id).First();
                if (dolceDisponibile != null)
                {
                    context.DolciDisponibili.Remove(dolceDisponibile);
                    context.SaveChanges();
                }
            }
        }
    }
}