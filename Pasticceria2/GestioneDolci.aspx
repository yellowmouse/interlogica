﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="GestioneDolci.aspx.cs" Inherits="Pasticceria2.GestioneDolci" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <h2>Dolci<a class='btn btn-success my-2' onclick="nuovoDolce()">Nuovo</a></h2>
                    <table id="divDolci"></table>
                    <div id="spinDolci" class="spinner-border text-primary" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <h3>Ingredienti<a class='btn btn-success my-2' onclick="nuovoIngrediente()">Nuovo</a></h3>
                    <table id="divIngredienti"></table>
                    <div id="spinIngredienti" class="spinner-border text-primary" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12">
                    <h2>Gestione Vetrina<a class='btn btn-success my-2' onclick="nuovoDolceDisponibile()">Nuovo</a></h2>
                    <table id="divDolciDisponibili"></table>
                    <div id="spinVetrina" class="spinner-border text-primary" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modalDolci" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modifica Dolce</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <input type="hidden" id="ID_Dolce">
                        <div class="row">
                            <div class="form-group">
                                <label for="Dolce_Nome">Nome</label>
                                <input type="text" class="form-control" id="Dolce_Nome" placeholder="Nome Dolce">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="Dolce_Prezzo">Prezzo Base</label>
                                <input type="number" min="0.00" max="10000.00" step="0.01" class="form-control" id="Dolce_Prezzo" placeholder="0.00">
                            </div>
                        </div>
                        <div class="row">
                            <h5>Ingredienti <a class='btn btn-success my-2' onclick="nuovoIngredienteDolce()">Nuovo</a></h5>
                        </div>
                        <div class="row">
                            <table id="ingredienti_dolce"></table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="salvaDolce()">Salva</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modalIngredienti" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modifica Ingrediente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <input type="hidden" id="ID_Ingrediente">
                            <div class="form-group">
                                <label for="Ingrediente_Nome">Nome</label>
                                <input type="text" class="form-control" id="Ingrediente_Nome" placeholder="Nome Ingrediente">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="salvaIngrediente()">Salva</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modalVetrina" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modifica Disponibilità Dolce</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <input type="hidden" id="ID_DolceDisponibile">
                        <div class="row">
                            <div class="form-group">
                                <label for="selectDolce">Dolce</label>
                                <div id="selectPickerDolce"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="Prezzo_Vendita">Prezzo</label>
                                <input type="number" class="form-control" id="Prezzo_Vendita" placeholder="0.00">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="Dolce_Quantita">Quantità</label>
                                <input type="number" class="form-control" id="Dolce_Quantita" placeholder="0">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="Data_Vendita">Data</label>
                                <input type="date" class="form-control" id="Data_Vendita" placeholder="">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="salvaDolceDisponibile()">Salva</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                </div>
            </div>
        </div>
    </div>



    <script src="GestioneDolci.js"></script>

</asp:Content>
