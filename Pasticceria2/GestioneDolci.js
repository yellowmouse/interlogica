﻿var ddl_ingredienti;
var ddl_dolci;

$(document).ready(async function () {

    leggiDolci();
    leggiIngredienti();
    leggiVetrina();

});

async function leggiDolci() {
    let resp = await ajaxGet('../api/Dolci', {});
    ddl_dolci = resp;
    let elems = resp.map((el) => {
        let elements = "<tr id='dolce" + el.ID_Dolce + "' ><td>" + el.Nome + "</td><td>" + el.Prezzo_Base + "€</td>" +
            "<td><a onClick='modificaDolce(" + el.ID_Dolce + ")' class='btn btn-primary my-2'>Edit</a>" +
            "<a onClick='eliminaDolce(" + el.ID_Dolce + ")' class='btn btn-danger my-2'>Cancella</a></td></tr>";
        return (elements);
    });
    if (elems.length > 0) {
        elems.unshift("<tr><td><strong>Nome</strong></td><td><strong>Prezzo Base</strong></td><td><strong>Op.</strong></td></tr>")
    }
    $("#divDolci").append(elems);
    $("#spinDolci").hide();
}

async function leggiIngredienti() {
    let resp = await ajaxGet('../api/Ingredienti', {});
    ddl_ingredienti = resp;
    let elems = resp.map((el) => {
        let elements = "<tr id='ingrediente" + el.ID_Ingrediente + "' ><td>" + el.Nome_Ingrediente + " </td>" +
            "<td><a onClick='modificaIngrediente(" + el.ID_Ingrediente + ")' class='btn btn-primary my-2'>Edit</a>" +
            "<a onClick='eliminaIngrediente(" + el.ID_Ingrediente + ")' class='btn btn-danger my-2'>Cancella</a></td></tr>";
        return (elements);
    });
    if (elems.length > 0) {
        elems.unshift("<tr><td><strong>Nome</strong></td><td><strong>Op.</strong></td></tr>")
    }
    $("#divIngredienti").append(elems);
    $("#spinIngredienti").hide();
}

async function leggiDolcixIngredienti(ID_Dolce) {
    let resp = await ajaxGet('../api/DolcixIngredienti/' + ID_Dolce, {});
    resp = JSON.parse(resp);
    let elems = resp.map((el) => {
        let elements = "<tr id='ingredientexDolce" + el.ID_Ingrediente + "' ><td><select class='selectpicker'>" + selectPickerIngrediente(el.ID_Ingrediente) + "</select></td>" +
            "<td><div class='form-control' ><input type='number' class='qta' value='" + el.Qta + "' /></div></td>" +
            "<td><div class='form-control' ><input type='text' class='udm' value='" + el.Udm + "' /></div></td>" +
            "<td><a onClick='eliminaIngredientexDolce(" + ID_Dolce + ", " + el.ID_Ingrediente + ")' class='btn btn-danger my-2'>Cancella</a></td></tr>";
        return (elements);
    });
    $("#ingredienti_dolce").append(elems);
}

function selectPickerIngrediente(ID_Ingrediente) {
    let selectDiv = $('<select class="selectpicker"></select>');

    let select = ddl_ingredienti.map((el) => {
        let elements = "<option value='" + el.ID_Ingrediente + "' " + (el.ID_Ingrediente == ID_Ingrediente ? "selected" : "") + ">" + el.Nome_Ingrediente + "</option>";
        return elements;
    });
    selectDiv.append(select);
    return selectDiv.html();
}

function selectPickerDolce(ID_Dolce) {
    let selectDiv = $('<select class="selectpicker"></select>');

    let select = ddl_dolci.map((el) => {
        let elements = "<option value='" + el.ID_Dolce + "' " + (el.ID_Dolce == ID_Dolce ? "selected" : "") + ">" + el.Nome + "</option>";
        return elements;
    });
    selectDiv.append(select);
    $("#selectPickerDolce").html("");
    $("#selectPickerDolce").append(selectDiv);
}

async function leggiIngrediente(ID_Ingrediente) {
    let resp = await ajaxGet('../api/Ingredienti/' + ID_Ingrediente, {});
    return resp;
}

async function leggiVetrina() {
    let resp = await ajaxGet('../api/DolciDisponibili', {});
    let elems = resp.map((el) => {
        let elements = "<tr id='DD" + el.ID + "' >" +
            "<td>" + el.Nome + "</td>" +
            "<td>" + el.Prezzo_Vendita + "€</td>" +
            "<td>" + el.Qta + " </td>" +
            "<td>" + new Date(el.Data_Vendita).toLocaleDateString("it-IT") + " </td>" +
            "<td><a onClick='modificaDolceDisponibile(" + el.ID + ")' class='btn btn-primary my-2'>Edit</a>" +
            "<a onClick='eliminaDolceDisponibile(" + el.ID + ")' class='btn btn-danger my-2'>Cancella</a></td></tr>";
        return (elements);
    });
    if (elems.length > 0) {
        elems.unshift("<tr><td><strong>Dolce</strong></td>" +
            "<td><strong>Prezzo</strong></td>" +
            "<td><strong>Qta</strong></td>" +
            "<td><strong>Data</strong></td>" +
            "<td><strong>Op.</strong></td></tr>")
    }
    $("#divDolciDisponibili").append(elems);
    $("#spinVetrina").hide();
}

function ajaxGet(url, data) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            data: data,
            success: (res) => resolve(res)
        });
    });
}

function nuovoDolce() {
    $("#ID_Dolce").val(0);
    $("#Dolce_Nome").val("");
    $("#Dolce_Prezzo").val("");
    $("#ingredienti_dolce").html("");
    $("#modalDolci").modal('show');
}

function nuovoIngrediente() {
    $("#ID_Ingrediente").val(0);
    $("#Ingrediente_Nome").val("");
    $("#modalIngredienti").modal('show');
}

function nuovoDolceDisponibile() {
    $("#ID_DolceDisponibile").val(0);
    selectPickerDolce(0);
    $("#modalVetrina").modal('show');
}

async function modificaDolce(ID_Dolce) {
    let resp = await ajaxGet('../api/Dolci/' + ID_Dolce, {});
    $("#ID_Dolce").val(ID_Dolce)
    $("#Dolce_Nome").val(resp.Nome);
    $("#Dolce_Prezzo").val(resp.Prezzo_Base);
    $("#ingredienti_dolce").html("");
    leggiDolcixIngredienti(ID_Dolce);
    $("#modalDolci").modal('show');
}

async function modificaIngrediente(ID_Ingrediente) {
    let resp = await ajaxGet('../api/Ingredienti/' + ID_Ingrediente, {});
    $("#ID_Ingrediente").val(ID_Ingrediente)
    $("#Ingrediente_Nome").val(resp.Nome_Ingrediente)
    $("#modalIngredienti").modal('show');
}

async function modificaDolceDisponibile(ID) {
    let resp = await ajaxGet('../api/DolciDisponibili/' + ID, {});
    resp = JSON.parse(resp);
    $("#ID_DolceDisponibile").val(ID)
    selectPickerDolce(resp.ID_Dolce);
    $("#Prezzo_Vendita").val(resp.Prezzo_Vendita);
    $("#Dolce_Quantita").val(resp.Qta);
    //$("#Data_Vendita").val(new Date(resp.Data_Vendita).toLocaleDateString("it-IT"));
    $("#Data_Vendita").val(resp.Data_Vendita.split("T")[0]);
    $("#modalVetrina").modal('show');
}

function eliminaDolce(ID_Dolce) {
    $.ajax({
        url: "../api/Dolci/" + ID_Dolce,
        type: "DELETE", // <- Change here
        contentType: "application/json",
        success: function () {
            $("#divDolci").html("");
            leggiDolci();
        },
        error: function () {
        }
    });
}

function eliminaIngrediente(ID_Ingrediente) {
    $.ajax({
        url: "../api/Ingredienti/" + ID_Ingrediente,
        type: "DELETE", // <- Change here
        contentType: "application/json",
        success: function () {
            $("#divIngredienti").html("");
            leggiIngredienti();
        },
        error: function () {
        }
    });
}

function eliminaIngredientexDolce(ID_Dolce, ID_Ingrediente) {
    $("#ingredientexDolce" + ID_Ingrediente).remove();
}

function eliminaDolceDisponibile(ID) {
    $.ajax({
        url: "../api/DolciDisponibili/" + ID,
        type: "DELETE", // <- Change here
        contentType: "application/json",
        success: function () {
            $("#divDolciDisponibili").html("");
            leggiVetrina();
        },
        error: function () {
        }
    });
}

function salvaIngrediente() {
    var parameter = {
        ID_Ingrediente: $("#ID_Ingrediente").val(),
        Nome_Ingrediente: $("#Ingrediente_Nome").val()
    };
    //$.post("../api/Ingredienti", JSON.stringify(parameter), function (data) {

    //}, "json");
    $.ajax({
        type: "POST",
        url: "../api/Ingredienti",
        data: { '': JSON.stringify(parameter) }
    }).done(function (data) {
        $("#modalIngredienti").modal('hide');
        $("#divIngredienti").html("");
        leggiIngredienti();
    }).error(function (jqXHR, textStatus, errorThrown) {
        alert(jqXHR.responseText || textStatus);
    });

}

function salvaDolce() {
    let ingredientiDolce = new Array();
    let arrIngrDom = $("#ingredienti_dolce tr")
    for (var i = 0; i < arrIngrDom.length; i++) {
        var ID_Ingrediente = $(arrIngrDom[i]).find("select")[0].value;
        var Qta = $($(arrIngrDom[i]).children("td")[1]).find("input")[0].value;
        var Udm = $($(arrIngrDom[i]).children("td")[2]).find("input")[0].value;
        ingredientiDolce.push({
            ID_Ingrediente: ID_Ingrediente,
            Qta: Qta,
            Udm: Udm
        });
    }
    var parameter = {
        ID_Dolce: $("#ID_Dolce").val(),
        Nome: $("#Dolce_Nome").val(),
        Prezzo_Base: $("#Dolce_Prezzo").val(),
        Ingredienti: ingredientiDolce
    };
    //$.post("../api/Ingredienti", JSON.stringify(parameter), function (data) {

    //}, "json");
    $.ajax({
        type: "POST",
        url: "../api/Dolci",
        data: { '': JSON.stringify(parameter) }
    }).done(function (data) {
        $("#modalDolci").modal('hide');
        $("#divDolci").html("");
        leggiDolci();
    }).error(function (jqXHR, textStatus, errorThrown) {
        alert(jqXHR.responseText || textStatus);
    });
}

function salvaDolceDisponibile() {
    var parameter = {
        ID: $("#ID_DolceDisponibile").val(),
        ID_Dolce: $("#modalVetrina").find("select")[0].value,
        Prezzo_Vendita: $("#Prezzo_Vendita").val(),
        Dolce_Quantita: $("#Dolce_Quantita").val(),
        Data_Vendita: $("#Data_Vendita").val()
    };
    //$.post("../api/Ingredienti", JSON.stringify(parameter), function (data) {

    //}, "json");
    $.ajax({
        type: "POST",
        url: "../api/DolciDisponibili",
        data: { '': JSON.stringify(parameter) }
    }).done(function (data) {
        $("#modalVetrina").modal('hide');
        $("#divDolciDisponibili").html("");
        leggiVetrina();
    }).error(function (jqXHR, textStatus, errorThrown) {
        alert(jqXHR.responseText || textStatus);
    });
}

function nuovoIngredienteDolce() {
    newLine = "<tr id='ingredientexDolce" + 0 + "' ><td><select class='selectpicker'>" + selectPickerIngrediente(0) + "</select></td>" +
        "<td><div class='form-control' ><input type='number' class='qta' value='" + 0 + "' /></div></td>" +
        "<td><div class='form-control' ><input type='text' class='udm' value='' /></div></td>" +
        "</tr>";
    $("#ingredienti_dolce").append(newLine);
}