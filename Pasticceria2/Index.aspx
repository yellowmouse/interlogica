﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Pasticceria2.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Luana e Maria Pasticceria</h1>
            <p class="lead text-muted">Benvenut* nel nostro sito.</p>
            <p>
                <a href="GestioneDolci.aspx" class="btn btn-primary my-2">Gestione Dolci</a>
                <a href="VetrinaDolci.aspx" class="btn btn-secondary my-2">Vetrina</a>
            </p>
        </div>
    </section>
</asp:Content>
