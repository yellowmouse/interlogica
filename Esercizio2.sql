﻿--1) Targa e Marca delle Auto di cilindrata superiore a 2000 cc o di potenza superiore a 120 CV
SELECT * FROM Auto WHERE Cilindrata > 2000 OR Potenza > 120


--2) Nome del proprietario e Targa delle Auto di cilindrata superiore a 2000 cc oppure di potenza
--superiore a 120 CV
SELECT Proprietari.Nome, Auto.Targa
FROM Auto
JOIN Proprietari ON Auto.CodF = Proprietari.CodF
WHERE Auto.Cilindrata > 2000 OR Auto.Potenza > 120


--3) Targa e Nome del proprietario delle Auto di cilindrata superiore a 2000 cc oppure di potenza
--superiore a 120 CV, assicurate presso la “SARA”
SELECT Auto.Targa, Proprietari.Nome
FROM Auto
JOIN Proprietari ON Auto.CodF = Proprietari.CodF
JOIN Assicurazioni ON Auto.CodAss = Assicurazioni.CodAss
WHERE Auto.Cilindrata > 2000 OR Auto.Potenza > 120 AND Assicurazioni.Nome = 'SARA'


--4) Targa e Nome del proprietario delle Auto assicurate presso la “SARA” e coinvolte in sinistri il
--20/01/02
SELECT DISTINCT Auto.Targa, Proprietari.Nome
FROM Auto
JOIN Proprietari ON Auto.CodF = Proprietari.CodF
JOIN Assicurazioni ON Auto.CodAss = Assicurazioni.CodAss
JOIN AutoCoinvolte ON Auto.Targa = AutoCoinvolte.Targa
JOIN Sinistri ON AutoCoinvolte.CodS = Sinistri.CodS
WHERE Assicurazioni.Nome = 'SARA' AND Sinistri.Data = '2020-01-02'

--5) Per ciascuna Assicurazione, il nome, la sede ed il numero di auto assicurate
SELECT Assicurazioni.Nome, Assicurazioni.Sede, COUNT(*) as N_Auto_Ass
FROM Assicurazioni
JOIN Auto ON Assicurazioni.CodAss = Auto.CodAss
GROUP BY Assicurazioni.Nome, Assicurazioni.Sede

--6) Per ciascuna auto “Fiat”, la targa dell’auto ed il numero di sinistri in cui è stata coinvolta
SELECT Auto.Targa, COUNT(AutoCoinvolte.CodS) as N_Sinistri
FROM Auto
LEFT JOIN AutoCoinvolte ON Auto.Targa = AutoCoinvolte.Targa
WHERE Auto.Marca = 'Fiat'
GROUP BY Auto.Targa

--7) Per ciascuna auto coinvolta in più di un sinistro, la targa dell’auto, il nome dell’Assicurazione, ed il
--totale dei danni riportati
SELECT Auto.Targa, Assicurazioni.Nome, COUNT(AutoCoinvolte.CodS), SUM(AutoCoinvolte.ImportoDelDanno)
FROM Auto
JOIN Assicurazioni ON Auto.CodAss = Assicurazioni.CodAss
JOIN AutoCoinvolte ON Auto.Targa = AutoCoinvolte.Targa
GROUP BY Auto.Targa, Assicurazioni.Nome
HAVING COUNT(AutoCoinvolte.CodS) > 1

--8) CodF e Nome di coloro che possiedono più di un’auto
SELECT Proprietari.CodF, Proprietari.Nome, COUNT(Auto.Targa)
FROM Proprietari
JOIN Auto ON Proprietari.CodF = Auto.CodF
GROUP BY Proprietari.CodF, Proprietari.Nome
HAVING COUNT(Auto.Targa) > 1

--9) La targa delle auto che non sono state coinvolte in sinistri dopo il 20/01/01
SELECT DISTINCT Auto.Targa
FROM Auto
LEFT JOIN AutoCoinvolte ON Auto.Targa = AutoCoinvolte.Targa 
LEFT JOIN Sinistri ON AutoCoinvolte.CodS = Sinistri.CodS AND Sinistri.Data > '2020-01-01'
WHERE Sinistri.CodS IS NULL


--10) Il codice dei sinistri in cui non sono state coinvolte auto con cilindrata inferiore a 2000 cc
SELECT DISTINCT AutoCoinvolte.CodS 
FROM AutoCoinvolte
JOIN Auto ON AutoCoinvolte.Targa = Auto.Targa AND Auto.Cilindrata > 2000