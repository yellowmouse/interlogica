﻿using Newtonsoft.Json;
using Pasticceria2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Pasticceria2.Controllers
{
    public class DolcixIngredientiController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            using (var context = new PASTICCERIAEntities())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;

                var query = (from i in context.Ingredienti
                             join dxi in context.DolcixIngredienti on i.ID_Ingrediente equals dxi.ID_Ingrediente
                             where dxi.ID_Dolce == id
                             select new {
                                 i.ID_Ingrediente,
                                 i.Nome_Ingrediente,
                                 dxi.Qta,
                                 dxi.Udm
                             }).ToList();

                string resp = JsonConvert.SerializeObject(query);

                return resp;
            }
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}