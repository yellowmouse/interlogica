﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Pasticceria2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Pasticceria2.Controllers
{
    public class DolciController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<Dolci> Get()
        {
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            using (var context = new PASTICCERIAEntities())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;

                var query = context.Dolci.ToList();
                return query;
            }
        }

        // GET api/<controller>/5
        public Dolci Get(int id)
        {
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            using (var context = new PASTICCERIAEntities())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;

                var query = context.Dolci.Where((elem) => elem.ID_Dolce == id).FirstOrDefault();
                return query;
            }
        }

        // POST api/<controller>
        public HttpResponseMessage Post([FromBody]string value)
        {
            var obj = JObject.Parse(value);
            using (var context = new PASTICCERIAEntities())
            {
                if (Int32.Parse(obj.GetValue("ID_Dolce").ToString()) == 0)
                {
                    //Inserimento

                    var dolce = new Dolci();
                    dolce.Nome = obj.GetValue("Nome").ToString();
                    dolce.Prezzo_Base = Double.Parse(obj.GetValue("Prezzo_Base").ToString());
                    context.Dolci.Add(dolce);
                    context.SaveChanges();

                    foreach (JObject elem in JArray.FromObject(obj.GetValue("Ingredienti")))
                    {
                        var dxi = new DolcixIngredienti();
                        dxi.ID_Dolce = dolce.ID_Dolce;
                        dxi.ID_Ingrediente = Int32.Parse(elem.GetValue("ID_Ingrediente").ToString());
                        dxi.Qta = Double.Parse(elem.GetValue("Qta").ToString());
                        dxi.Udm = elem.GetValue("ID_Ingrediente").ToString();
                        context.DolcixIngredienti.Add(dxi);
                    }
                    context.SaveChanges();

                }
                else
                {
                    //Modifica
                    var id = Int32.Parse(obj.GetValue("ID_Dolce").ToString());
                    var dolce = context.Dolci.Where((elem) => elem.ID_Dolce  == id).First();
                    dolce.Nome = obj.GetValue("Nome").ToString();
                    dolce.Prezzo_Base = Double.Parse(obj.GetValue("Prezzo_Base").ToString());

                    var ingredientixDolce = context.DolcixIngredienti.Where((elem) => elem.ID_Dolce == id).ToList();
                    context.DolcixIngredienti.RemoveRange(ingredientixDolce);
                    context.SaveChanges();

                    foreach (JObject elem in JArray.FromObject(obj.GetValue("Ingredienti"))){
                        var dxi = new DolcixIngredienti();
                        dxi.ID_Dolce = id;
                        dxi.ID_Ingrediente = Int32.Parse(elem.GetValue("ID_Ingrediente").ToString());
                        dxi.Qta = Double.Parse(elem.GetValue("Qta").ToString());
                        dxi.Udm = elem.GetValue("ID_Ingrediente").ToString();
                        context.DolcixIngredienti.Add(dxi);
                    }
                    context.SaveChanges();

                }
            }
            return new HttpResponseMessage()
            {
                Content = new StringContent("Dolce creato")
            };
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            using (var context = new PASTICCERIAEntities())
            {
                var dolce = context.Dolci.Where((ing) => ing.ID_Dolce  == id).First();
                var ingredienti = context.DolcixIngredienti.Where((ing) => ing.ID_Dolce == id).ToList();
                if (dolce != null)
                {
                    context.DolcixIngredienti.RemoveRange(ingredienti);
                    context.Dolci.Remove(dolce);
                    context.SaveChanges();
                }
            }
        }
    }
}