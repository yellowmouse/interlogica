﻿var ddl_ingredienti;

$(document).ready(async function () {

    leggiVetrina();
    leggiIngredienti();

});

function ajaxGet(url, data) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            data: data,
            success: (res) => resolve(res)
        });
    });
}

async function leggiVetrina() {
    let resp = await ajaxGet('../api/DolciDisponibili', {});
    let today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

    let elems = resp.filter((el) => {
        let difference = today - new Date(el.Data_Vendita);
        let days = difference / (1000 * 3600 * 24);
        return days <= 2;
    }).map((el) => {

        let difference = today - new Date(el.Data_Vendita);
        let days = difference / (1000 * 3600 * 24);
        switch (days) {
            case 0:
                el.scontato = false;
                break;
            case 1:
                el.scontato = true;
                el.sconto = "20%";
                el.Prezzo_Vendita = el.Prezzo_Vendita * 0.8;
                break;
            case 2:
                el.scontato = true;
                el.sconto = "80%";
                el.Prezzo_Vendita = el.Prezzo_Vendita * 0.2;
                break;
        }

        let elementsDiv = "<div class='col-md-3 mb-5'>";
        if (el.scontato) {
            elementsDiv += "<span class='badge badge-warning'>" + el.sconto + "</span>";
        }
        elementsDiv += "<div class='card border-info mx-sm-1 p-3'>"
        if (el.scontato) {
            elementsDiv += "<div class='card border-info shadow text-info p-3 my-card'>" + el.Prezzo_Vendita + "€</div>"
        } else {
            elementsDiv += "<div class='card border-info shadow text-info p-3 my-card'>" + el.Prezzo_Vendita + "€</div>"
        }
        elementsDiv += "<div class='text-info text-center mt-3'><h4>" + el.Nome + "</h4></div>" +
            "<div class='text-info text-center mt-3'>Quantità disponibile: " + el.Qta + "</div>" +
            "<a onClick='IngredientiTorta(" + el.ID_Dolce + ");' class='btn btn-primary my-2'><strong>Info</strong></a>" +
            "</div>" +
            "</div>"

        return (elementsDiv);
    });
    //if (elems.length > 0) {
    //    elems.unshift("<tr><td><strong>Dolce</strong></td>" +
    //        "<td><strong>Prezzo</strong></td>" +
    //        "<td><strong>Qta</strong></td>" +
    //        "<td><strong>Data</strong></td>" +
    //        "<td><strong>Op.</strong></td></tr>")
    //}
    $("#divDolciDisponibili").append(elems);
    $("#spinVetrina").hide(); 
}

async function IngredientiTorta(ID_Dolce) {
    $("#lista_ingredienti_dolce").html("");
    let resp = await ajaxGet('../api/DolcixIngredienti/' + ID_Dolce, {});
    resp = JSON.parse(resp);
    resp = resp.map((elem) => { return elem.ID_Ingrediente; });
    let ingredientiDolce = ddl_ingredienti.filter((elem) => {
        return resp.includes(elem.ID_Ingrediente);
    })
    let elems = ingredientiDolce.map((el) => {
        let elements = "<li>" + el.Nome_Ingrediente  + "</li>";
        return (elements);
    });
    $("#lista_ingredienti_dolce").append(elems);
    $("#modalDolci").modal("show")
}


async function leggiIngredienti() {
    let resp = await ajaxGet('../api/Ingredienti', {});
    ddl_ingredienti = resp;
}